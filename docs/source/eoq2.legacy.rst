eoq2.legacy package
===================

Submodules
----------

eoq2.legacy.legacy module
-------------------------

.. automodule:: eoq2.legacy.legacy
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: eoq2.legacy
   :members:
   :undoc-members:
   :show-inheritance:
