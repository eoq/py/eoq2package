.. eoq2 documentation master file, created by
   sphinx-quickstart on Wed May 13 11:59:55 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to eoq2's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   eoq2


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
