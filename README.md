# pip Package Creator: EOQ2 Python Library

This is the pip package and creation repository for the [EOQ2 Python library](https://gitlab.com/eoq/py/eoq2), which belongs to [PyEOQ](https://gitlab.com/eoq/py/pyeoq). See links for more details on the python implementation of EOQ2.

# EOQ

[Essential Object Query (EOQ)](https://gitlab.com/eoq/essentialobjectquery) is a language to interact remotely and efficiently with object-oriented models, i.e. domain-specific models. It explicitly supports the search for patterns, as used in model transformation languages. Its motivation is an easy to parse and deterministically behaving query structure, but as high as possible efficiency and flexibility. EOQ’s capabilities and semantics are similar to the Object-Constraint-Language (OCL), but it supports in addition transactional model modification, change events, and error handling.  

Main Repository: https://gitlab.com/eoq/essentialobjectquery

EOQ user manual: https://gitlab.com/eoq/doc 


