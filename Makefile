all: pull package upload

pull:
	cd eoq2/ && git pull origin master

package:
	python3 setup.py sdist --formats=zip
	python3 setup.py bdist_wheel

upload:
	twine upload  dist/*.zip
